#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXTHREAD 32

struct th_param {
    int thrid;
    int prio;
};

unsigned long int contador[MAXTHREAD];
unsigned long int lecturas[MAXTHREAD];
pthread_t threads[MAXTHREAD];
int prioridades[MAXTHREAD];


void * worker(void *arg) {
    struct th_param * param = (struct th_param *) arg;
    if (!param) {
        printf("Error en los parametros del thread\n");
        pthread_exit(NULL);
    }
    int prio = param->prio;
    int thrid = param->thrid;

   	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL); // to be able to cancel this thread at any point

    setpriority(PRIO_PROCESS, 0, prio);
    printf("Creando thread %d con nice: %d\n", thrid, prioridades[thrid]=getpriority(PRIO_PROCESS, 0));

    while (1) {
        contador[thrid]++;
    }
}

int main(int argc, char **argv)
{
    int nthreads = 2;
    int i;
    int j;
    struct th_param *parametros;

    if (argc > 1) {
        nthreads = atoi(argv[1]);
    }
    if (nthreads < 1) {
        nthreads = 2;
    }
    else if (nthreads > 32) {
        nthreads = 32;
    }

    for (i=0; i<nthreads; i++) {
        lecturas[i] = 0;
        if (argc-i >= 3) {
            prioridades[i] = atoi(argv[2+i]);
        } else {
            prioridades[i] = 0;           
        }
        if (prioridades[i] < -20) {
            prioridades[i] = -20;
        }
        else if (prioridades[i] > 19) {
            prioridades[i] = 19;
        }
    }

    printf("Creando %d threads ...\n", nthreads); fflush(NULL);
    for (i=0; i<nthreads; i++) {
        parametros = (struct th_param *) malloc (sizeof(struct th_param));
        parametros->prio = prioridades[i];
        parametros->thrid = i;
        pthread_create(&threads[i], NULL, worker, parametros);
    }

    for (j=0; j<5; j++) {
        sleep(1);
        printf("%d s\n", j);
        for (i=0; i<nthreads; i++) {
            printf("contador[%d] = %12lu\n", i, contador[i]);
        }
    }

    for (i=0; i<nthreads; i++) {
        printf("Cancelando thread %d ...\n", i);
        pthread_cancel(threads[i]);
    }

    printf("Resultados: \n");
    unsigned long int total = 0;
    for (i=0; i<nthreads; i++) total+=contador[i];
    
    for (i=0; i<nthreads; i++) {
        printf("contador[%d] = %12lu (%5.2f %%)  %11s prio: %d nice: %2d\n", i, contador[i], (double)contador[i]/(double)total*100, "SCHED_OTHER", 0, prioridades[i]);
    }
    return 0;
}
