#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXTHREAD 32

char *politicas[5]= {"SCHED_OTHER","SCHED_FIFO", "SCHED_RR", "SCHED_BATCH","SCHED_IDLE" };


struct th_param {
    int thrid;
    int prio;
    int politica_real;
    int prio_real;
};

unsigned long int contador[MAXTHREAD];
unsigned long int lecturas[MAXTHREAD];
pthread_t threads[MAXTHREAD];
int prioridades[MAXTHREAD];
struct th_param parametros[MAXTHREAD];

void * worker(void *arg) {
    struct th_param * param = (struct th_param *) arg;
    struct sched_param sp;
    if (!param) {
        printf("Error en los parametros del thread\n");
        pthread_exit(NULL);
    }
    int prio = param->prio;
    int thrid = param->thrid;
    int tipo;
    pthread_t myid=pthread_self();
    
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL); // to be able to cancel this thread at any point

    printf("Creando thread %2d con prioridad: %2d, politica: %s\n", thrid, (prio==1)?0:prio, politicas[(prio==1)?0:2] );

    sp.sched_priority=prio;
    
	
	if(prio==1) 
	{
		param->prio_real=0;
		param->politica_real=SCHED_OTHER;
		sp.sched_priority=0; 
		pthread_setschedparam(myid, SCHED_OTHER, &sp); 
		
	} // time sharing priority 
	else
	{
		param->prio_real=prio;
		param->politica_real=SCHED_RR;
		pthread_setschedparam(myid, SCHED_RR, &sp);  // real time priority
    }
	
	pthread_getschedparam(myid, &tipo, &sp); 
    
    printf("->LISTO thread %2d con prioridad: %2d, politica: %s\n", thrid, sp.sched_priority, politicas[tipo] );
    param->prio_real=sp.sched_priority;
	param->politica_real=tipo;

    while (1) {
        contador[thrid]++;
      //   if(contador[thrid]==100000) break;
        if(contador[thrid]==10000000000) break;  // salida del bucle por si las moscas, en real-time podemos dejar la máquina colgada
    }
   
}

int main(int argc, char **argv)
{
    int nthreads = 2;
    int i;
    int j;
    struct th_param *params;
    struct sched_param sp;
    
    sp.sched_priority=98; // la prioridad del thread principal debe ser alta para poder cancelar a los otros
   	pthread_setschedparam(pthread_self(), SCHED_FIFO, &sp);

    if (argc > 1) {
        nthreads = atoi(argv[1]);
    }
    if (nthreads < 1) {
        nthreads = 2;
    }
    else if (nthreads > 32) {
        nthreads = 32;
    }

    for (i=0; i<nthreads; i++) {
        lecturas[i] = 0;
        if (argc-i >= 3) {
            prioridades[i] = atoi(argv[2+i]);
        } else {
            prioridades[i] = 50;           
        }
        if (prioridades[i] < 1) {
            prioridades[i] = 1;
        }
        else if (prioridades[i] > 99) {
            prioridades[i] = 99;
        }
    }

    printf("Creando %d threads ...\n", nthreads); fflush(NULL);
    for (i=0; i<nthreads; i++) {
        params = &parametros[i];
        params->prio = prioridades[i];
        params->thrid = i;
        pthread_create(&threads[i], NULL, worker, params);
    }

    for (j=0; j<5; j++) {
        sleep(1);
        printf("%d s\n", j);
        for (i=0; i<nthreads; i++) {
            printf("contador[%d] = %12lu\n", i, contador[i]);
        }
    }

    for (i=0; i<nthreads; i++) {
        printf("Cancelando thread %d ...\n", i);
        pthread_cancel(threads[i]);
    }

    printf("Resultados: \n");
    unsigned long int total = 0;
    for (i=0; i<nthreads; i++) total+=contador[i];
    
    for (i=0; i<nthreads; i++) {
        printf("Thread:contador[%d] = %12lu (%5.2f %%)  %11s prio: %2d\n", i, contador[i], (double)contador[i]/(double)total*100,
        politicas[parametros[i].politica_real],parametros[i].prio_real);
    }
    return 0;
}
