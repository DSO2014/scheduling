PROGRAMAS= nice_prio rt_prio
FLAGS = -pthread

compilar_programas : $(PROGRAMAS)

% : %.c 
	gcc -o $@ $< $(FLAGS)

real-time-on: 
	sudo sh -c "echo -1 > /proc/sys/kernel/sched_rt_runtime_us"

real-time-off: 
	sudo sh -c "echo 950000 > /proc/sys/kernel/sched_rt_runtime_us"
